package com.company.SimpleCheckSum;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.company.SimpleCheckSum.MD5Checksum;


/**
 * Hello world!
 */
public final class App {
	  static final Logger logger = LogManager.getLogger(App.class.getName());
	   
    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
       // System.out.println("Hello World!");
  
        JSONParser parser = new JSONParser();

        try {     
            Object obj = parser.parse(new FileReader("src/config/default.json"));

            JSONObject jsonObject =  (JSONObject) obj;

            //Reading From Config
            String urlName = (String) jsonObject.get("url");
            String md5_url = (String) jsonObject.get("md5_url");
            String filepath = (String) jsonObject.get("filePath");
            String fileName = (String) jsonObject.get("fileName");
            String md5fileName = (String) jsonObject.get("md5FileName");
          //  System.out.println("Url Name"+urlName+"\n"+"md5 url"+md5_url);
            logger.debug("Url Name"+urlName+"\n"+"md5 url"+md5_url);
            System.out.println("Url Name"+urlName+"\n"+"md5 url"+md5_url);
            
            // Downloading Files 
            JavaDownloadFileFromURL downloadurl = new JavaDownloadFileFromURL();
            downloadurl.downloadUsingStream(urlName,fileName);
            downloadurl.downloadUsingNIO(md5_url, md5fileName);
            
            
            //Checking MD5 
            try {
                MD5Checksum dl = new MD5Checksum();
                String calMd5 = dl.getMD5Checksum(fileName);
                logger.debug("generated MD5  "+calMd5);
                System.out.println("generated MD5  "+calMd5);
               
                String array1[]= readLineByLineJava8(md5fileName).toString().split("=");
                String insidemd5 = array1[1].toString();
                System.out.println(  calMd5+" see "+insidemd5.replaceAll("\\s","") );
                if (insidemd5.replaceAll("\\s","").equals(calMd5) )
                {
                	System.out.println("Success");
                }
                else {
                	System.out.println("Checksum is not matching");
                }
            }catch(Exception e) {
            	e.printStackTrace();
            }
            
            
            
            System.exit(0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }  
    }
    
    
    public static String readLineByLineJava8(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();
 
        try (Stream<String> stream =Files.lines(Paths.get(filePath)))
        {
            stream.forEach(s -> {
            	try {
            		contentBuilder.append(s).append("\n");
            	}catch (Exception e) {
                    e.printStackTrace();
                }
             
            }
            );
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
 
        return contentBuilder.toString();
    }
}
