package com.company.SimpleCheckSum;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
 
public class ReadFileToString
{
    public static void main(String[] args)
    {   try {
        MD5Checksum dl = new MD5Checksum();
        String calMd5 = dl.getMD5Checksum("E://Dubai/apache_1.3.20.tar.gz.md5");
        String filePath = "E://Dubai/apache_1.3.20.tar.gz";
        //String checksum = "some string";
        System.out.println( readLineByLineJava8( filePath ) );
       
        String array1[]= readLineByLineJava8(filePath).toString().split("=");
        String insidemd5 = array1[1].toString();
        System.out.println(  calMd5+" see "+insidemd5.replaceAll("\\s","") );
        if (insidemd5.replaceAll("\\s","").equals(calMd5) )
        {
        	System.out.println("Success");
        }
        else {
        	System.out.println("Checksum is not matching");
        }
    }catch(Exception e) {
    	e.printStackTrace();
    }
    }
 
 
    //Read file content into string with - Files.lines(Path path, Charset cs)
 
    public static String readLineByLineJava8(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();
 
        try (Stream<String> stream = Files.lines( Paths.get(filePath)))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
 
        return contentBuilder.toString();
    }
}