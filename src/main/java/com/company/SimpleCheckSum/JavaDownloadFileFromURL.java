package com.company.SimpleCheckSum;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class JavaDownloadFileFromURL {

	// public static void main(String[] args) {
	// 	String url = "https://archive.apache.org/dist/httpd/apache_1.3.20.tar.gz";
	// 	String url2 = "https://archive.apache.org/dist/httpd/apache_1.3.20.tar.gz.md5";
	// 	try {
	// 		downloadUsingStream(url, "E://Dubai/apache_1.3.20.tar.gz.md5");
	// 		downloadUsingNIO(url2, "E://Dubai/apache_1.3.20.tar.gz");
			
	// 	} catch (IOException e) {
	// 		e.printStackTrace();
	// 	}
	// }



	  static void downloadUsingStream(String urlStr, String file) throws IOException{
		URL url = new URL(urlStr);
		BufferedInputStream bis = new BufferedInputStream(url.openStream());
		FileOutputStream fis = new FileOutputStream(file);
		byte[] buffer = new byte[1024];
		int count=0;
		while((count = bis.read(buffer,0,1024)) != -1)
		{
			fis.write(buffer, 0, count);
		}
		fis.close();	
		bis.close();
	}

	  static void downloadUsingNIO(String urlStr, String file) throws IOException {
		URL url = new URL(urlStr);
		ReadableByteChannel rbc = Channels.newChannel(url.openStream());
		FileOutputStream fos = new FileOutputStream(file);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		fos.close();
		rbc.close();
	}
}
